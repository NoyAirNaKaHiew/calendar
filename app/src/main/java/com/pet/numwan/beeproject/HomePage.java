package com.pet.numwan.beeproject;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.zip.Inflater;

public class HomePage extends Fragment {
    private static final String TAG = "HomePage";
    View view;
    CalendarView calendarView;
    Spinner timeSt;
    Spinner timed;
    private EditText topic;
    private EditText Place;
    private EditText many_people;
    String time_start;
    String time_end;
    String date;
    FirebaseDatabase mData;
    DatabaseReference databaseReference;
    DatabaseReference number_month;
    Button btn_save;
    Button btn_remove;
    String tomonth;
    String toyears;
    String today;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
       view = inflater.inflate(R.layout.activity_home_page,container,false);
        calendarView = (CalendarView) view.findViewById(R.id.cal_create);
        timeSt = (Spinner) view.findViewById(R.id.time_start);
        timed = (Spinner) view.findViewById(R.id.time_end);
        topic = (EditText) view.findViewById(R.id.name_of);
        Place = (EditText) view.findViewById(R.id.place);
        many_people = (EditText) view.findViewById(R.id.many_people);
        btn_save =(Button) view.findViewById(R.id.btn_save);
        btn_remove = (Button) view.findViewById(R.id.btn_delete);

        mData = FirebaseDatabase.getInstance();
        databaseReference = mData.getReference("Event");
        number_month = mData.getReference("Month");

        final ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(),R.array.time,android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        timeSt.setAdapter(adapter);
        timed.setAdapter(adapter);
        timeSt.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                time_start = adapterView.getItemAtPosition(i).toString();

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        timed.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                time_end = adapterView.getItemAtPosition(i).toString();

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        calendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(@NonNull CalendarView calendarView, int years, int month, int day) {
                int j = month+1;
                date = day+"on"+j+"in"+years;
                //Toast.makeText(getActivity(),tomonth,Toast.LENGTH_LONG).show();
                databaseReference.child(date).addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if (dataSnapshot.exists()) {
                           try {
                               String name = dataSnapshot.child("Name").getValue().toString();
                               String place = dataSnapshot.child("Place").getValue().toString();
                               String many = dataSnapshot.child("Many").getValue().toString();
                               String timestart = dataSnapshot.child("time_start").getValue().toString();
                               String timeend = dataSnapshot.child("time_end").getValue().toString();
                               topic.setText(name);
                               Place.setText(place);
                               many_people.setText(many);
                               timeSt.setSelection(adapter.getPosition(timestart));
                               timed.setSelection(adapter.getPosition(timeend));
                           }catch (Exception e){

                           }
                        }
                        else {
                            topic.setText("");
                            Place.setText("");
                            many_people.setText("");
                            timeSt.setSelection(0);
                            timed.setSelection(0);
                            Toast.makeText(getActivity(),"No event",Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
            }
        });

        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               final String topic_name = topic.getText().toString();
               final String place_name = Place.getText().toString();
               final String many = many_people.getText().toString();
               final String timest = time_start;
               final String timeen = time_end;
               final DatabaseReference newPost = databaseReference.child(date);
               newPost.child("Name").setValue(topic_name);
               newPost.child("Place").setValue(place_name);
               newPost.child("Many").setValue(many);
               newPost.child("time_start").setValue(timest);
               newPost.child("time_end").setValue(timeen);
               Toast.makeText(getActivity(),"Success",Toast.LENGTH_LONG).show();

            }
        });

        btn_remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                topic.setText("");
                Place.setText("");
                many_people.setText("");
                timeSt.setSelection(0);
                timed.setSelection(0);
            }
        });
       return view;
    }
}
