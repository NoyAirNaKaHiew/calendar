package com.pet.numwan.beeproject;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class LoginPage extends AppCompatActivity {
    private Button btn_login;
    FirebaseAuth mAuth;
    EditText email_lo;
    EditText pass_lo;
    FirebaseDatabase database;
    DatabaseReference databaseReference;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_page);
        getSupportActionBar().hide();

        mAuth = FirebaseAuth.getInstance();

        email_lo = (EditText) findViewById(R.id.email_login);
        pass_lo = (EditText) findViewById(R.id.pass_login);

        database = FirebaseDatabase.getInstance();
        databaseReference = database.getReference("Login");

        final String today = new SimpleDateFormat("dd-M-yyyy", Locale.getDefault()).format(new Date());
        final String[] Today = today.split("-");
        String todayy = Today[0]+"on"+Today[1]+"in"+Today[2];

        btn_login = (Button) findViewById(R.id.login_btn);
        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final DatabaseReference mData = databaseReference.child(Today[2]).child(Today[1]).child(Today[0]);
                String email = email_lo.getText().toString();
                String pass = pass_lo.getText().toString();
                final DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
                final Calendar cal = Calendar.getInstance();
                mAuth.signInWithEmailAndPassword(email,pass)
                        .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if (task.isSuccessful()) {
                                    // Sign in success, update UI with the signed-in user's information
                                    Log.d("Login", "signInWithEmail:success");
                                    mData.child(dateFormat.format(cal.getTime())).setValue("1");
                                    Intent intent = new Intent(getApplicationContext(),MainActivity.class);
                                    startActivity(intent);


                                } else {
                                    // If sign in fails, display a message to the user.
                                    Log.w("Login", "signInWithEmail:failure", task.getException());
                                    Toast.makeText(getApplicationContext(), "Authentication failed.",
                                            Toast.LENGTH_SHORT).show();
                                    DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
                                    Calendar cal = Calendar.getInstance();
                                    Toast.makeText(getApplication(),dateFormat.format(cal.getTime()),Toast.LENGTH_LONG).show();
                                }
                            }
                        });

            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

        FirebaseUser user = mAuth.getCurrentUser();
        if(user != null){
            Intent intent = new Intent(getApplicationContext(),MainActivity.class);
            startActivity(intent);
        }
        else{
            Log.d("Login", "Not Login");
        }
    }
}
