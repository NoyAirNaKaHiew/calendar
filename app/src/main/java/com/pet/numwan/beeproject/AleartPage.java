package com.pet.numwan.beeproject;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.CalendarView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

public class AleartPage extends AppCompatActivity {
    TextView topic;
    TextView place2;
    TextView many2;
    TextView timest;
    TextView timeed;
    FirebaseDatabase database;
    DatabaseReference databaseReference;
    String key;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aleart_page);
        key = getIntent().getExtras().getString("getDate");

        database = FirebaseDatabase.getInstance();
        databaseReference= database.getReference("Event");

        topic = (TextView) findViewById(R.id.topic_bmr);
        place2 = (TextView) findViewById(R.id.place_bmr);
        many2 = (TextView) findViewById(R.id.number_bmr);
        timest = (TextView) findViewById(R.id.time_start_bmr);
        timeed = (TextView) findViewById(R.id.time_end_bmr);

        databaseReference.child(key).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
               if(!dataSnapshot.child("Name").getValue().equals("")) {
                   String name = dataSnapshot.child("Name").getValue().toString();
                   String place = dataSnapshot.child("Place").getValue().toString();
                   String many = dataSnapshot.child("Many").getValue().toString();
                   String timestart = dataSnapshot.child("time_start").getValue().toString();
                   String timeend = dataSnapshot.child("time_end").getValue().toString();

                   topic.setText("เรื่อง \n" + name);
                   place2.setText("สถานที่ \n" + place);
                   many2.setText("จำนวน " + many + " คน");
                   timest.setText("เริ่ม " + timestart + "-" + timeend);
               }
               else if(dataSnapshot.child("Name").getValue().equals("")){
                   topic.setText("ไม่มีข้อมูล");
                   place2.setText("");
                   many2.setText("");
                   timest.setText("");
               }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }
}
