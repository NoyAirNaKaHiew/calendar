package com.pet.numwan.beeproject;

import android.content.Intent;
import android.graphics.YuvImage;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import org.w3c.dom.Text;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class InfomationPage extends Fragment {
    private static final String TAG = "Information";
    View view;
    CalendarView calen;
    String date;
    TextView numtoday;
    TextView numYesterday;
    TextView thismonth;
    TextView Lastmonth;
    TextView ThisYears;
    FirebaseDatabase database;
    DatabaseReference databaseReference;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.activity_infomation_page,container,false);
        calen = (CalendarView) view.findViewById(R.id.calen_show);
        numtoday = (TextView) view.findViewById(R.id.num_today);
        numYesterday = (TextView) view.findViewById(R.id.num_Yesterday);
        thismonth = (TextView) view.findViewById(R.id.num_month_5);
        Lastmonth = (TextView) view.findViewById(R.id.num_last_month);
        ThisYears = (TextView) view.findViewById(R.id.num_Years);

        calen.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(@NonNull CalendarView calendarView, int years, int month, int day) {
                int j = month+1;
                date = day+"on"+j+"in"+years;
                Intent intent = new Intent(getActivity(),AleartPage.class);
                intent.putExtra("getDate",date);
                startActivity(intent);


            }
        });

        database = FirebaseDatabase.getInstance();
        databaseReference = database.getReference("Login");

        final String today = new SimpleDateFormat("dd-M-yyyy", Locale.getDefault()).format(new Date());
        final String[] Today = today.split("-");
        final int yesterday = Integer.valueOf(Today[0])-1;

       databaseReference.child(Today[2]).child(Today[1]).child(Today[0]).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()) {
                    int k = 0;
                    for (DataSnapshot dsp : dataSnapshot.getChildren()) {
                        k += 1;
                    }
                    numtoday.setText(String.valueOf(k));
                }else {}
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
       databaseReference.child(Today[2]).child(Today[1]).child(String.valueOf(yesterday)).addValueEventListener(new ValueEventListener() {
           @Override
           public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
               if(dataSnapshot.exists()){
                   int i = 0;
                   for (DataSnapshot dsp : dataSnapshot.getChildren()) {
                       i += 1;
                   }
                   numYesterday.setText(String.valueOf(i));
               }
               else{

               }

           }

           @Override
           public void onCancelled(@NonNull DatabaseError databaseError) {

           }
       });
        databaseReference.child(Today[2]).child(Today[1]).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()) {
                    int m = 0;
                    for (DataSnapshot snap : dataSnapshot.getChildren()) {
                        m += snap.getChildrenCount();
                    }
                    thismonth.setText(String.valueOf(m));
                }else {

                }

            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        final int lastmonth = Integer.valueOf(Today[1])-1;
        databaseReference.child(Today[2]).child(String.valueOf(lastmonth)).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    int a = 0;
                    for (DataSnapshot dsp : dataSnapshot.getChildren()) {
                        a += dsp.getChildrenCount();
                    }
                    Lastmonth.setText(String.valueOf(a));
                }
                else{

                }

            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        databaseReference.child(Today[2]).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    int i = 0;
                    for (DataSnapshot dsp : dataSnapshot.getChildren()) {
                        for(DataSnapshot npp: dsp.getChildren()){
                          i += npp.getChildrenCount();
                        }
                    }
                    ThisYears.setText(String.valueOf(i));
                }
                else{

                }

            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        return view;
    }
}
